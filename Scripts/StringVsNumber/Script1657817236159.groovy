import internal.GlobalVariable as GlobalVariable

assert GlobalVariable.StringVar instanceof String

assert GlobalVariable.NumVar instanceof Integer

